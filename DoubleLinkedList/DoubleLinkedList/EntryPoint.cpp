#include "DoubleLinkList.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

void Print(DoubleLinkList<int> &list)
{
	DoubleLinkList<int>::Node* testNode = list.start;
	while (testNode != nullptr)
	{
		std::cout << testNode->data;
		if (testNode->next)
		{
			std::cout << ", ";
		}
		testNode = testNode->next;
	};
	std::cout << std::endl << std::endl;
}

int main()
{
	{
		DoubleLinkList<int> test1;
		test1.PushBack(1);
		Print(test1);
		test1.PushBack(2);
		Print(test1);
		test1.PushBack(3);
		Print(test1);
		test1.PushBack(4);
		Print(test1);
		test1.PushBack(5);
		Print(test1);
		test1.PushBack(6);
		Print(test1);
		test1.PushBack(7);
		Print(test1);
		test1.PushBack(8);
		Print(test1);
		test1.PushBack(9);
		Print(test1);
		test1.PushFront(0);
		Print(test1);
		test1.Insert(test1.start->next->next->next, 11);
		Print(test1);
		test1.Erase(test1.start->next->next->next->next->next);
		Print(test1);
		test1.Sort([](int a, int b) {return a < b; });
		Print(test1);
		std::cout << "Press enter to exit" << std::endl;
		std::cin.get();
	}
	_CrtDumpMemoryLeaks();
}