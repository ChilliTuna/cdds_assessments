#pragma once
#include <iostream>

template <typename T>
class DoubleLinkList
{
public:
	class Node
	{
	public:
		T data;
		Node* previous = nullptr;
		Node* next = nullptr;

		Node(T input)
		{
			data = input;
		}
	};

	Node* start = nullptr;
	Node* end = nullptr;

	void PushFront(T input)
	{
		std::cout << "Pushed front " << input << std::endl;
		Node* nodeThingy = new Node(input);
		if (start != nullptr)
		{
			start->previous = nodeThingy;
			nodeThingy->next = start;
			start = nodeThingy;
		}
		else
		{
			start = nodeThingy;
			end = nodeThingy;
		}
	}

	void PushBack(T input)
	{
		std::cout << "Pushed back " << input << std::endl;
		Node* nodeThingy = new Node(input);
		if (end != nullptr)
		{
			end->next = nodeThingy;
			nodeThingy->previous = end;
			end = nodeThingy;
		}
		else
		{
			start = nodeThingy;
			end = nodeThingy;
		}
	}

	void Insert(Node* node, T input)
	{
		Node* nodeThingy = new Node(input);
		Node* searchNode = start;
		while (searchNode != node)
		{
			searchNode = searchNode->next;
			if (searchNode == nullptr)
			{
				std::cout << "Entered node is not part of list" << std::endl;
				return;
			}
			if (searchNode == node)
			{
				break;
			}
		}
		std::cout << "Inserted " << input << " before " << node->data << std::endl;
		if (node->previous == nullptr)
		{
			PushFront(input);
		}
		else
		{
			nodeThingy->previous = node->previous;
			nodeThingy->next = node;
			node->previous->next = nodeThingy;
			node->previous = nodeThingy;
		}
	}

	void PopFront()
	{
		std::cout << "Popped front " << start->data << std::endl;
		Node* tempPointer = start;
		start = start->next;
		start->previous = nullptr;
		delete tempPointer;
	}

	void PopBack()
	{
		std::cout << "Popped back " << end->data << std::endl;
		Node* tempPointer = end;
		end = end->previous;
		end->next = nullptr;
		delete tempPointer;
	}

	void Erase(Node* removedNode)
	{
		if (start == removedNode)
		{
			PopFront();
		}
		else if (end == removedNode)
		{
			PopBack();
		}
		else
		{
		std::cout << "Removed " << removedNode->data << std::endl;
			removedNode->next->previous = removedNode->previous;
			removedNode->previous->next = removedNode->next;
			delete removedNode;
		}
	}

	void Remove(T value)
	{
		Node* searchNode = start;
		while (searchNode != nullptr)
		{
			if (searchNode->data == value)
			{
				Erase(searchNode);
				searchNode = start;
			}
			searchNode = searchNode->next;
		}
	}

	void Swap(Node* a, Node* b)
	{
		if (start == a)
		{
			start = b;
		}
		else
		{
			a->previous->next = b;
		}
		if (end == b)
		{
			end = a;
		}
		else
		{
			b->next->previous = a;
		}
		b->previous = a->previous;
		a->next = b->next;
		a->previous = b;
		b->next = a;
	}

	void Sort(bool (*compFunction)(T a, T b))
	{
		std::cout << "Sorted list" << std::endl;
		int numSwaps;
		Node* node;
		do
		{
			numSwaps = 0;
			node = start;
			while (end != node && node != nullptr)
			{
				if (compFunction(node->data, node->next->data))
				{
					Swap(node, node->next);
					numSwaps++;
				}
				node = node->next;
			}
		} while (numSwaps > 0);
	}

	int GetSize()
	{
		int size = 0;
		Node* currentNode = start;
		while (currentNode != nullptr)
		{
			size++;
			currentNode = currentNode->next;
		}
		return size;
	}

	Node* GetStart()
	{
		return start;
	}

	Node* GetEnd()
	{
		return end;
	}

	bool IsEmpty()
	{
		return start == nullptr;
	}

	~DoubleLinkList()
	{
		Node* currentPointer = end;
		do
		{
			Node* prev = currentPointer->previous;
			delete currentPointer;
			currentPointer = prev;
		} while (currentPointer->previous != nullptr);
		delete start;
	}

	Node* operator[](unsigned int index)
	{
		if (index >= GetSize())
		{
			return nullptr;
		}
		Node* tempNode = start;
		for (int i = 0; i < index; i++)
		{
			tempNode = tempNode->next;
		}
		return tempNode;
	}
};