#include "Game.h"
#include "BinaryTree.h"
#include <string>
#include <vector>
#include <sstream>	//String stream, used for the FPS counter

BinaryTree tree;
int currentValue = 0;
double pointInTime = 0;
std::string printText = "";
bool shouldPrintText = false;

void Game::Init()
{
	SetTargetFPS(60);

	int maxRange = 10;

	tree.Add(maxRange / 2);

	for (int i = 1; i < maxRange * 3; i++)
	{
		tree.Add(GetRandomValue(0, maxRange));
	}
}

void Game::Shutdown()
{
}

void Game::Update()
{
	if (tree.needsPositioning)
	{
		tree.AssignPositions(tree.head);
		tree.needsPositioning = false;
	}
	deltaTime = GetFrameTime();

	testPosition.x += 100 * deltaTime;

	if (IsKeyPressed(KEY_W))
	{
		currentValue += 5;
	}
	else if (IsKeyPressed(KEY_A))
	{
		currentValue--;
	}
	else if (IsKeyPressed(KEY_S))
	{
		currentValue -= 5;
	}
	else if (IsKeyPressed(KEY_D))
	{
		currentValue++;
	}
	else if (IsKeyPressed(KEY_ENTER))
	{
		tree.Add(currentValue);
	}
	else if (IsKeyPressed(KEY_BACKSPACE))
	{
		tree.Remove(currentValue);
	}
	else if (IsKeyPressed(KEY_F))
	{
		bool isFound = tree.CheckForVal(currentValue);
		std::string foundText;
		isFound ? foundText = "" : foundText = "not";
		printText = "";
		printText.append(std::to_string(currentValue));
		printText.append(" was ");
		printText.append(foundText);
		printText.append(" found");
		pointInTime = GetTime();
		shouldPrintText = true;
	}
}

void Game::Draw()
{
	std::stringstream fpsCounter;

	fpsCounter << "FPS: " << GetFPS();

	BeginDrawing();	//Rendering code comes after this call...
	ClearBackground(DARKBLUE);

	tree.DrawTree();

	if (shouldPrintText)
	{
		DrawText(printText.c_str(), (int)((float)GetScreenWidth() / 2.5), GetScreenHeight() / 2, 30, RED);
		if (GetTime() - pointInTime > 2)
		{
			shouldPrintText = false;
		}
	}

	DrawText(std::to_string(currentValue).c_str(), GetScreenWidth() / 2, (int)(GetScreenHeight() * 0.8), 50, BLACK);
	DrawText("A/D increase/decrease by 1 | W/D increase/decrease by 5", (int)(GetScreenWidth() / 3.2), (int)(GetScreenHeight() * 0.93), 19, BLACK);
	DrawText("F check if value exists | ENTER to add value | BACKSPACE to remove value", GetScreenWidth() / 4, (int)(GetScreenHeight() * 0.97), 19, BLACK);

	DrawText(fpsCounter.str().c_str(), 10, 10, 20, RED);

	EndDrawing();	//...and before this one.
}