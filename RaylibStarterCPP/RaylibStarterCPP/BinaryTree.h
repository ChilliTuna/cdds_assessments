#pragma once

#include "raylib.h"
#include "Game.h"
#include <vector>
#include <iostream>
#include <string>

class BinaryTree
{
public:
	struct Node
	{
		Node* parent = nullptr;
		Node* lChild = nullptr;
		Node* rChild = nullptr;
		int data;

		int depth = 0;
		Vector2 position = { 0, 0 };

		Node(int data)
		{
			this->data = data;
		}

		int GetDepth(int depth)
		{
			int leftDepth = 0;
			int rightDepth = 0;
			this->depth = depth;
			if (lChild)
			{
				leftDepth = lChild->GetDepth(depth + 1);
			}
			if (rChild)
			{
				rightDepth = rChild->GetDepth(depth + 1);
			}
			return (leftDepth > rightDepth ? leftDepth : rightDepth) + 1;
		}

		void DrawNodes(int maxDepth)
		{
			DrawCircle((int)position.x, (int)position.y, WINDOW_WIDTH / ((float)maxDepth * 20) + 5, BLACK);
			DrawCircle((int)position.x, (int)position.y, WINDOW_WIDTH / ((float)maxDepth * 20), DARKGREEN);
			std::string text = std::to_string(data);
			const char* charText = text.c_str();
			DrawText(charText, (int)position.x - 5 * text.length(), (int)position.y - 45, 24, WHITE);
			if (lChild)
			{
				lChild->DrawNodes(maxDepth);
			}
			if (rChild)
			{
				rChild->DrawNodes(maxDepth);
			}
		}

		void DrawLines()
		{
			if (parent)
			{
				DrawLine((int)position.x, (int)position.y, (int)parent->position.x, (int)parent->position.y, BLACK);
			}
			if (lChild)
			{
				lChild->DrawLines();
			}
			if (rChild)
			{
				rChild->DrawLines();
			}
		}
	};

	Node* head = nullptr;
	bool needsPositioning = true;
	int maxDepth = 1;

	~BinaryTree()
	{
		while (head != nullptr)
		{
			Remove(head);
		}
	}

	void AssignPositions(Node* node)
	{
		if (head == node)
		{
			maxDepth = node->GetDepth(0);
			node->position.x = WINDOW_WIDTH / 2;
		}
		node->position.y = WINDOW_HEIGHT * (float)(node->depth + 1) / (float)(maxDepth + 1);
		if (node->lChild)
		{
			node->lChild->position.x = (float)node->position.x - (float)(WINDOW_WIDTH / pow(2, node->depth + 2));
			AssignPositions(node->lChild);
		}
		if (node->rChild)
		{
			node->rChild->position.x = (float)node->position.x + (float)(WINDOW_WIDTH / pow(2, node->depth + 2));
			AssignPositions(node->rChild);
		}
		needsPositioning = true;
	}

	void DrawTree()
	{
		if (head)
		{
			AssignPositions(head);
			head->DrawLines();
			head->DrawNodes(maxDepth);
		}
	}

	void Add(int data)
	{
		Node* newNode = new Node(data);
		if (head == nullptr)
		{
			head = newNode;
			return;
		}
		Node* searchNode = head;
		while (true)
		{
			if (newNode->data < searchNode->data)
			{
				if (searchNode->lChild == nullptr)
				{
					searchNode->lChild = newNode;
					newNode->parent = searchNode;
					return;
				}
				searchNode = searchNode->lChild;
			}
			else if (newNode->data > searchNode->data)
			{
				if (searchNode->rChild == nullptr)
				{
					searchNode->rChild = newNode;
					newNode->parent = searchNode;
					return;
				}
				searchNode = searchNode->rChild;
			}
			else
			{
				//std::cout << "Oops, that data couldn't be added because it was a duplicate of a preexisting node" << std::endl;
				delete newNode;
				return;
			}
		}
	}

	void Remove(int data)
	{
		if (head == nullptr)
		{
			return;
		}
		Node* searchNode = head;
		while (true)
		{
			if (data < searchNode->data)
			{
				searchNode = searchNode->lChild;
			}
			else if (data > searchNode->data)
			{
				searchNode = searchNode->rChild;
			}
			else if (data == searchNode->data)
			{
				PerformDelete(searchNode);
				return;
			}
			if (searchNode == nullptr)
			{
				//std::cout << "The tree does not contain a node with that data" << std::endl;
				return;
			}
		}
	}

	void Remove(Node* node)
	{
		if (head == nullptr)
		{
			return;
		}
		while (true)
		{
			if (CheckForVal(node->data))
			{
				PerformDelete(node);
				return;
			}
			else
			{
				//std::cout << "The tree does not contain a node with that data" << std::endl;
				return;
			}
		}
	}

	void PerformDelete(Node* node)
	{
		//Deletes leaf
		if (node->lChild == nullptr && node->rChild == nullptr)
		{
			DeleteLeaf(node);
		}
		//Deletes 1 child node
		else if (node->lChild != nullptr && node->rChild == nullptr)
		{
			DeleteOneChildNode(node, node->lChild);
		}
		else if (node->lChild == nullptr && node->rChild != nullptr)
		{
			DeleteOneChildNode(node, node->rChild);
		}
		//Deletes 2 child node
		else if (node->lChild != nullptr)
		{
			DeleteTwoChildNode(node);
		}
	}

	void DeleteLeaf(Node* node)
	{
		if (head == node)
		{
			head = nullptr;
		}
		else if (node->parent)
		{
			if (node->data < node->parent->data)
			{
				node->parent->lChild = nullptr;
			}
			else if (node->data > node->parent->data)
			{
				node->parent->rChild = nullptr;
			}
		}
		delete node;
	}

	void DeleteOneChildNode(Node* node, Node* child)
	{
		if (head == node)
		{
			head = child;
		}
		else if (node->parent)
		{
			if (node->data < node->parent->data)
			{
				node->parent->lChild = child;
			}
			else if (node->data > node->parent->data)
			{
				node->parent->rChild = child;
			}
		}
		child->parent = node->parent;
		delete node;
	}

	void DeleteTwoChildNode(Node* node)
	{
		Node* swapNode = GetSmallestChild(node->rChild);
		if (swapNode->parent != node)
		{
			if (swapNode->parent != nullptr)
			{
				swapNode->parent->lChild = swapNode->rChild;
			}
			if (swapNode->rChild)
			{
				swapNode->rChild->parent = swapNode->parent;
			}
			swapNode->rChild = node->rChild;
			node->rChild->parent = swapNode;
		}
		swapNode->parent = node->parent;
		if (node->parent)
		{
			if (node->data < node->parent->data)
			{
				node->parent->lChild = swapNode;
			}
			else if (node->data > node->parent->data)
			{
				node->parent->rChild = swapNode;
			}
		}
		else
		{
			head = swapNode;
		}
		swapNode->lChild = node->lChild;
		node->lChild->parent = swapNode;
		delete node;
	}

	Node* GetSmallestChild(Node* node)
	{
		if (node->lChild != nullptr)
		{
			return GetSmallestChild(node->lChild);
		}
		return node;
	}

	Node* FetchNode(int data)
	{
		if (CheckForVal(data))
		{
			Node* searchNode = head;
			while (true)
			{
				if (data < searchNode->data)
				{
					searchNode = searchNode->lChild;
				}
				else if (data > searchNode->data)
				{
					searchNode = searchNode->rChild;
				}
				else return searchNode;
			}
		}
		else return nullptr;
	}

	bool CheckForVal(int data)
	{
		Node* searchNode = head;
		while (true)
		{
			if (data < searchNode->data)
			{
				searchNode = searchNode->lChild;
			}
			else if (data > searchNode->data)
			{
				searchNode = searchNode->rChild;
			}
			else if (data == searchNode->data)
			{
				return true;
			}
			if (searchNode == nullptr)
			{
				return false;
			}
		}
	}
};