#include "raylib.h"
#include "Game.h"

int main()
{
	Game game;

	InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Binary Tree");

	game.Init();

	while (!WindowShouldClose())
	{
		game.Update();
		game.Draw();
	}

	game.Shutdown();
	CloseWindow();
} 