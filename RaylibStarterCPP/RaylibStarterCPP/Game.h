#pragma once

#include "raylib.h"

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720

class Game
{
private:

	float deltaTime = 0;

	Vector2 testPosition = { 0, 0 };
public:
	Game() {}

	void Init();

	void Shutdown();

	void Update();

	void Draw();

};