#include "HashFunction.h"

namespace HashFunction
{
	// implementation of a basic addition hash
	unsigned int BadHash(const char* data, unsigned int length)
	{
		unsigned int hash = 0;
		for (unsigned int i = 0; i < length; ++i)
		{
			hash += data[i];
		}
		return hash;
	}

	unsigned int NotAsBadHash(const char* data, unsigned int length)
	{
		unsigned int hash = 0;
		for (unsigned int i = 0; i < length; ++i)
		{
			hash = (hash * 1313) + data[i];
		}
		return hash;
	}
}