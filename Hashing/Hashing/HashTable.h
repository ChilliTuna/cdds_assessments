#pragma once
#include <vector>
#include <string>
#include <fstream>
#include "HashFunction.h"
#define HASH_TABLE_SIZE 100

typedef std::vector<char> Datum;

static Datum ReadFile(std::string filename)
{
    std::fstream fileInput;
    fileInput.open(filename, std::ios_base::in | std::ios_base::binary);
    Datum newData;
    while (fileInput.peek() != EOF)
    {
        char thisCharacter;
        fileInput.read(&thisCharacter, 1);
        newData.push_back(thisCharacter);
    }
    return newData;
}

class HashTable
{
private:
    Datum data[HASH_TABLE_SIZE];

public:
    void AddData(std::string fileName, Datum fileContents)
    {
        int index = HashFunction::NotAsBadHash(fileName.c_str(), fileName.size()) % HASH_TABLE_SIZE;
        data[index] = fileContents;
    }

    Datum GetData(int key)
    {
        return data[key % HASH_TABLE_SIZE];
    }
};

