#include "HashFunction.h"
#include "HashTable.h"
#include <iostream>

int main()
{
	HashTable hTable;
	std::string fileName = "HashTest.dat";
	Datum data = ReadFile(fileName);
	hTable.AddData(fileName, data);

	std::cout << "Hash key:" << std::endl;
	std::cout << HashFunction::NotAsBadHash(fileName.c_str(), fileName.size()) << std::endl;;
	std::cout << std::endl;
	std::cout << "Value:" << std::endl;

	Datum data2 = hTable.GetData(HashFunction::NotAsBadHash(fileName.c_str(), fileName.size()));
	for (char character : data2)
	{
		std::cout << character;
	}
	std::cout << std::endl;
	system("pause");
}