#pragma once
#include <functional>
#include <string>

namespace HashFunction
{
	typedef std::function< unsigned int(const char*, unsigned int)> HashFunc;
	// implementation of a basic addition hash
	unsigned int BadHash(const char* data, unsigned int length);
	// ADD YOUR FUNCTIONS HERE
	unsigned int NotAsBadHash(const char* data, unsigned int length);
	// a helper to access a default hash function
	static HashFunc defaultHash = NotAsBadHash;
}